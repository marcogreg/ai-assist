FROM golang:1.22-alpine

RUN apk add --no-cache git make
WORKDIR /app

COPY . .

RUN go build .

EXPOSE 8080

ENTRYPOINT ["/app/ai-assist"]
