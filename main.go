package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"

	pb "gitlab.com/schin1/ai-assist/protos/headermirror"
)

// server is used to implement example.HeaderServiceServer.
type server struct {
	pb.UnimplementedHeaderServiceServer
}

// GetHeaders implements example.HeaderServiceServer
func (s *server) GetHeaders(ctx context.Context, in *pb.Empty) (*pb.HeaderResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return &pb.HeaderResponse{Headers: map[string]string{}}, nil
	}

	headers := make(map[string]string)
	for key, values := range md {
		headers[key] = values[0] // Assuming single value for simplicity
	}

	return &pb.HeaderResponse{Headers: headers}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterHeaderServiceServer(s, &server{})

	healthServer := health.NewServer()
	healthpb.RegisterHealthServer(s, healthServer)
	healthServer.SetServingStatus("headermirror.HeaderService", healthpb.HealthCheckResponse_SERVING)

	reflection.Register(s)

	log.Printf("Server listening on port 50051")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
